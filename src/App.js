import React from "react";
import David from "./assets/images/david_miller.png";

import "./App.scss";

function App() {
  return (
    <div>
      <h1>Welcome to React App</h1>
      <img src={David} alt="David Warner" />
    </div>
  );
}

export default App;
